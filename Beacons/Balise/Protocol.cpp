#include "Protocol.h"
#include "Def.h"
#include "Types.h"
#include "Sensors.h"
#include "SDLib.h"
#include <TinyGPS++.h>

TinyGPSPlus gps;

//Serial port of ASP

/*start serial session*/
int serial_start(HardwareSerial *port, int baud) {
  port->begin(baud);
  if(*port)  return 1;
  else        return 0;
}
//---------------------------------------------------------------------------------------------------------------
// THOSE FUNCTIONS TAKECARE OF RECEPTION OF DATA, CONVERTING THEM TO LITTLE INDIAN
///This function return a normal read byte
byte readByte(HardwareSerial *port) {
  union i_tag {
    byte b[1];
    byte ival;
  } i;
  i.b[0] = port->read();
  return i.ival;
}

///This function reads 2 bytes and convert them into an integer
int readInt(HardwareSerial *port) {
  union i_tag {
    byte b[2];
    int ival;
  } i;
  i.b[0] = port->read();
  i.b[1] = port->read();
  return i.ival;
}

///This function reads 4 bytes and convert them into an unsigned long
unsigned long readLong(HardwareSerial *port) {
  union i_tag {
    byte b[4];
    unsigned long ival;
  } i;
  i.b[0] = port->read();
  i.b[1] = port->read();
  i.b[2] = port->read();
  i.b[3] = port->read();
  return i.ival;
}

///This function reads 4 bytes and convert them into a float
float readFloat(HardwareSerial *port) {
  union i_tag {
    byte b[4];
    float ival;
  } i;
  i.b[0] = port->read();
  i.b[1] = port->read();
  i.b[2] = port->read();
  i.b[3] = port->read();
  return i.ival;
}
//---------------------------------------------------------------------------------------------------------------
int read_Frame(ASP_GET * frame, HardwareSerial *port) {
  while(port->available() >= GET_SIZE){
    // fill array
    frame->pack_preamble_1 = readByte(port);
    frame->pack_preamble_2 = readByte(port);
    frame->pack_direction  = readByte(port);
    frame->balise_id       = readInt(port);
    frame->pack_command    = readInt(port);
    frame->pack_crc        = readInt(port);
  }
  #ifdef VERBOSE_MODE
      display_GET(frame);
  #endif
  Serial3.flush();
  return SUCCESS;
}

/*this method handles the received frame*/
int analyze_Frame(ASP_GET * frame, ASP_GPS * gps) {
  if( !((frame->pack_preamble_1 == '$') && (frame->pack_preamble_2 == 'B')) )
      return FALSE_FRAME;
  if(!(frame->pack_direction == '>'))
      return FALSE_DIRECTION;
  if((frame->balise_id == 1))   /* 1 for broadast by airplane*/
  {
      send_GPS(gps);
      return SUCCESS;
  }
  /*if( !(frame->pack_crc == check_CRC(frame)) )
      return FALSE_CRC;*/
  if(!(frame->balise_id == BALISE_ID))
      return FALSE_ID;
  //Here, the frame is correct
  switch(frame->pack_command) {
      //case ASP_ID: send_DATA(ASP_ID, ASP_ID); break;
      case ASP_TEMP: send_DATA(ASP_TEMP, get_Temp()); break;
      case ASP_HUMIDITY: send_DATA(ASP_HUMIDITY, get_Humidity()); break;
      case ASP_GPS_DATA: send_GPS(gps); break;
      case ASP_SLEEP:    sleep_Balise(); break;
      case ASP_DUMP:     dump_File(); break;
  }
  //frame->pack_direction = 60;
  return SUCCESS;
}

//---------------------------------------------------------------------------------------------------------------
int refresh_GPS(ASP_GPS * frame, GPS_Date * gpd, GPS_Time * gpt) {
  int hour = 0;
  while (Serial2.available() > 0)
    gps.encode(Serial2.read());
  frame->pack_preamble_1 = '$';
  frame->pack_preamble_2 = 'B';
  frame->pack_direction  = '<';
  frame->balise_id = BALISE_ID;
  frame->pack_command = ASP_GPS_DATA;
  /*frame->gps_lat = 6.2;
  frame->gps_long = 7.5;
  frame->gps_alt = 1.4;*/
  #ifdef MANUAL_GPS
    frame->gps_lat = GPS_LAT;
    frame->gps_long = GPS_LONG;
    frame->gps_alt = GPS_ALT;
  #else
    frame->gps_lat = gps.location.lat();
    frame->gps_long = gps.location.lng();
    frame->gps_alt = gps.altitude.meters();
  #endif
  frame->pack_crc = 5;
  //Date and time
  gpd->year = gps.date.year();
  gpd->month = gps.date.month();
  gpd->day = gps.date.day();
  hour = (gps.time.hour() + TIME_ZONE) % 24;
  gpt->hour = hour;
  gpt->minute = gps.time.minute();
  gpt->second = gps.time.second();
  Serial2.flush();
  return SUCCESS;
}

int send_GPS(ASP_GPS * frame) {
  union ASP_Serialize{
  ASP_GPS frame;
  byte stream[GPS_SIZE];
  } ser;
  ser.frame = *frame;
  Serial3.write(ser.stream,GPS_SIZE);
  Serial3.flush();
  return SUCCESS;
}

int send_ACK() {
  union ASP_Serialize{
  ASP_DATA frame;
  byte stream[GET_SIZE];
  } pack;
  pack.frame.pack_preamble_1 = '$';
  pack.frame.pack_preamble_2 = 'B';
  pack.frame.pack_direction  = '<';
  pack.frame.balise_id = BALISE_ID;
  pack.frame.pack_command = ASP_ACK;
  pack.frame.pack_crc = 5;
  Serial3.write(pack.stream,GET_SIZE);
  Serial3.flush();
  return SUCCESS;
}

int send_DATA(int command, float data) {
  union ASP_Serialize{
  ASP_DATA frame;
  byte stream[DATA_SIZE];
  } pack;
  pack.frame.pack_preamble_1 = '$';
  pack.frame.pack_preamble_2 = 'B';
  pack.frame.pack_direction  = '<';
  pack.frame.balise_id = BALISE_ID;
  pack.frame.pack_command = command;
  pack.frame.pack_data = data;
  pack.frame.pack_crc = 5;
  Serial3.write(pack.stream,DATA_SIZE);
  Serial3.flush();
  return SUCCESS;
}
//---------------------------------------------------------------------------------------------------------------
int free_GET(ASP_GET * frame) {
  frame->pack_preamble_1 = 0;
  frame->pack_preamble_2 = 0;
  frame->pack_direction = 0;
  frame->balise_id = 0;
  frame->pack_command = 0;
  frame->pack_crc = 0;
  return SUCCESS;
}

//---------------------------------------------------------------------------------------------------------------
void display_GET(ASP_GET * frame) {
    Serial.println("-----------------------------------------------------------------");
    Serial.print("The received frame: ");
    Serial.print("<");
    Serial.print(frame->pack_preamble_1);
    Serial.print(" ");
    Serial.print(frame->pack_preamble_2);
    Serial.print(" ");
    Serial.print(frame->pack_direction);
    Serial.print(" ");
    Serial.print(frame->balise_id);
    Serial.print(" ");
    Serial.print(frame->pack_command);
    Serial.print(" ");
    Serial.print(frame->pack_crc);
    Serial.println(">");
    Serial.println("-----------------------------------------------------------------");
}

void display_GPS(ASP_GPS * frame) {
    Serial.print("The GPS data: ");
    //Serial.println("<lat\tlong\talt>");
    Serial.print("<LAT=");
    Serial.print(frame->gps_lat);
    Serial.print("\tLONG=");
    Serial.print(frame->gps_long);
    Serial.print("\tALT=");
    Serial.print(frame->gps_alt);
    Serial.println(">");
}

void display_TEMP(float temp) {
  Serial.print("The  temperature is: ");
  Serial.print(temp);
  Serial.print(" ");
  Serial.println("°C");
}

void display_HUMIDITY(float humid) {
  Serial.print("The  humidity is: ");
  Serial.print(humid);
  Serial.print(" ");
  Serial.println("%");
}

void display_DATE(GPS_Date * d) {
  Serial.print("Today is: ");
  Serial.print(d->day);
  Serial.print("/");
  Serial.print(d->month);
  Serial.print("/");
  Serial.println(d->year);
}

void display_TIME(GPS_Time * t) {
  Serial.print("Time is: ");
  Serial.print(t->hour);
  Serial.print(":");
  Serial.print(t->minute);
  Serial.print(":");
  Serial.println(t->second);
}

// DELAY FOR LONG TIME IN ORDER TO STOP FUNCTIONALITY OF THE TAG, IN FUTURE UPGRADE IT WILL BE REPLACED BY WATCHDOG
void sleep_Balise() {
  delay(SLEEP_SECONDS*1000);
}
