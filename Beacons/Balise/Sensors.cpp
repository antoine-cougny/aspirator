#include "Sensors.h"
#include "Def.h"

//Temp sensors
/*
*WARNING:
*The pin corresponding to temperature sensor is defined in "Def.h"
*in config list
*/

OneWire oneWire(ONE_WIRE_BUS); 
DallasTemperature sensors(&oneWire);

float temp = 0.0;

//Humidity
int sensorValue = 0;     // value read from the pot
int humidite = 0;

int start_Sensors() {
  sensors.begin();
  return SUCCESS;
}

float get_Temp() {
  sensors.requestTemperatures();
  //delay(100);
  temp = sensors.getTempCByIndex(0);
  return temp;
}

float get_Humidity() {
  // wait 2 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(2);
  sensorValue = analogRead(HUMIDITY_PIN); ///defined in "Def.h"
  humidite = map(sensorValue,0,1024,0,100); //map(value, fromlow, fromhigh; tolow, tohigh)
  return (100 - humidite);
}
