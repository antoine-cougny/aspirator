#ifndef SD_H_
#define SD_H_

#include "Arduino.h"
#include "Def.h"
#include <SPI.h>
#include <SD.h>

int start_SD();
int free_SD();
int save_Data(String data);
int dump_File();

#endif /* SD_H_ */
