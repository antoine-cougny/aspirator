#include "SD.h"
#include "Def.h"

File myFile;
File dumpFile;
String fname = "data_";
String filename = fname + BALISE_ID + ".txt";

int start_SD() {
  if (!SD.begin(SD_PIN))
      return FAIL;
  return SUCCESS;
}

int free_SD() {
  SD.remove(filename);
}

int save_Data(String data) {
  myFile = SD.open(filename, FILE_WRITE);
  if (myFile) {
    myFile.println(data);
    myFile.close();
    return SUCCESS;
  }
  else  return FAIL;
}

// TO SEND ALL THE CONTENT OF THE FILE BY SERIAL
int dump_File() {
  Serial3.flush();
  dumpFile = SD.open(filename);
  if (dumpFile) {
    while (dumpFile.available()) {
      Serial3.write(dumpFile.read());
    }
    dumpFile.close();
    Serial3.flush();
    return SUCCESS;
  }
  else {
    return FAIL; 
  }
}

