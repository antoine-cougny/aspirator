#!/usr/bin/python3
# -*- coding: Utf-8 -*-

import numpy as np 
import os
import matplotlib.pyplot as plt

class Avion():
    def __init__(self,b,xt1,yt1,xt2,yt2):
        self.i = 0      # ième balise en cours
        self.list_b = b # liste des balises
        self.b = b[0]   # balise en cours
        self.x1 = xt1   # avion instant t1
        self.y1 = yt1
        self.x2 = xt2   # avion instant t2 qui correspond à l'instant présent
        self.y2 = yt2
        self.b.etat = "non_detectee"
    
    def choix_balise(self):
        d = 999999
#        portee = 1
        for b in self.list_b:
            try :
                db= np.sqrt((b.x-self.x2)**2 + (b.y-self.y2)**2) #distance par rapport à la balise
                
                if b.etat == "données récupérées":
                    b.etat = b.etat
                                   
                elif (db <= d and b.etat == "detectee"):
                    d = db
                    self.b = b #La balise en cours devient la balise la plus proche

            except:
                continue
        

    def correction(self,ListeBalise):
        epsilon = 0.0001
        portee = 0.001

        try:
            d1 = np.sqrt((self.b.x-self.x1)**2 + (self.b.y-self.y1)**2)
            d2 = np.sqrt((self.b.x-self.x2)**2 + (self.b.y-self.y2)**2)

            if self.b.etat == "non_detectee":
                d = 10**3
                for b in self.list_b :
                    db= np.sqrt((b.x-self.x2)**2 + (b.y-self.y2)**2)
                    if (db <= d ):
                        d = db
                        self.b = b
                   
#            if self.b.id in IDbeacon :
            if self.b.etat != "données récupérées":
                self.b.etat = "en_cours"
#            print(self.list_b)
            for b in self.list_b:   #On regarde si de nouvelles balises sont détectables avec la nouvelle position de l'avion
                db= np.sqrt((b.x-self.x2)**2 + (b.y-self.y2)**2)
#                print(b.id)
#                if (db < portee and b.etat == "non_detectee"):
                for i in range(len(ListeBalise)):
                    if b.id == ListeBalise[i].id and b.etat =="non_detectee" :
#                if b.id in ListeBalise :
                        b.etat = "detectee"
   
            if self.b.etat == "données récupérées":
#                self.b.etat = "données récupérées"
#                recuperer self.b.donnee
                print('informations collectées')
 #               os.system('espeak "informations collectées" -v french -s 130 -p 50')
                return 1    #On sort du programme, plus besoin de guidage
                if self.i < len(self.list_b) - 1: # s'il reste des balises
                    self.i += 1
                    self.choix_balise()
    #                self.b = self.list_b[self.i]
#                    self.b.etat = "en_cours"
                else:
                    self.i += 1
                    print('fin de mission')
#                    os.system('espeak "fin de mission" -v french -s 130 -p 50')
                    

                    
            v1 = np.array([self.b.x-self.x1, self.b.y-self.y1,0])
            v2 = np.array([self.b.x-self.x2, self.b.y-self.y2,0])
            v3 = np.cross(v1,v2) #produit vectoriel
            di = np.inner(v3,[0,0,1]) #produit scalaire
            
            
            if self.x1 == self.x2 and self.y1 == self.y2:
                print('on ne bouge plus')
                os.system('espeak "on ne bouge plus" -v french -s 130 -p 50')
            else:

                if di > 0:
                    print('plus à gauche')
                    os.system('espeak "plus à gauche" -v french -s 130 -p 50')
                elif di < 0:
                    print('plus à droite')
                    os.system('espeak "plus à droite" -v french -s 130 -p 50')
                elif di==0 and d2<d1:
                    print('tout droit')
                    os.system('espeak "tout droit" -v french -s 130 -p 50')
                elif di==0 and d2>=d1:
                    print('demi tour')
                    os.system('espeak "demi tour" -v french -s 130 -p 50')
        
                if d1 < d2:
                    print('''on s'éloigne''')
                    # os.system('espeak "on éloigne" -v french -s 130 -p 50')

        except TypeError:
            pass
