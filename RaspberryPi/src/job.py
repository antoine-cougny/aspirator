#! /usr/bin/env python3
# coding:utf-8

class Job(object):
    def __init__(self, priority, description):
        self.priority = priority
        self.description = description
        #print('New job:', description)
        return
    def __cmp__(self, other):
        ## For Python 2.7
        return cmp(self.priority, other.priority)

    def __lt__(self, other):
        ## For Python 3
        return (self.priority < other.priority)

    def __eq__(self, other):
        ## For Python 3
        return (self.priority == other.priority)

if __name__ == '__main__':

    ## Tests unitaires
    A = Job(2, 'test A prio 2')
    B = Job(3, 'test B prio 3')
    C = Job(1, 'test C prio 1')
    D = Job(5, 'test D prio 5')
    E = Job(1, 'test E prio 1b')
    print(A>B, A<B)

    ## Priority Queue
    try:
        import Queue            # Python 2.7
    except:
        import queue as Queue   # Python 3.3

    q = Queue.PriorityQueue(10)
    q.put(A)
    q.put(B)
    q.put(C)
    q.put(D)
    q.put(E)
    while not q.empty():
        print(q.get().description)
