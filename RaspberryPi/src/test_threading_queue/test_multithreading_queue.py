#! /usr/bin/env python3
# coding:utf-8

import serial
import threading
import os
import struct
from time import sleep

try:
    import Queue            # Python 2.7
except:
    import queue as Queue   # Python 3.3

class Aspoa(threading.Thread):
    RxQ = Queue.Queue()
    stop = threading.Event()

    ###################################################
    ###                                             ###
    ###              INITIALISATION                 ###
    ###                                             ###
    ###################################################
    def __init__(self,state):
        threading.Thread.__init__(self)
        self.state = state # producer or consomer 

    def shutdown(self):
        self.stop.set()
        self.join()
        print("Closing the serial port !")


